defmodule MockAppWeb.Router do
  use MockAppWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", MockAppWeb do
    pipe_through :api
    resources "/user_status", UserStatusController, except: [:new, :edit]
  end
end
