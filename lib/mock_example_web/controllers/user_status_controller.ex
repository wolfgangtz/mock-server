defmodule MockAppWeb.UserStatusController do
  use MockAppWeb, :controller

  alias MockApp.Status
  alias MockApp.Status.UserStatus

  action_fallback MockAppWeb.FallbackController

  def index(conn, _params) do
    user_status = Status.list_user_status()
    render(conn, "index.json", user_status: user_status)
  end

  def create(conn, %{"user_status" => user_status_params}) do
    with {:ok, %UserStatus{} = user_status} <- Status.create_user_status(user_status_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.user_status_path(conn, :show, user_status))
      |> render("show.json", user_status: user_status)
    end
  end

  def show(conn, %{"id" => id}) do
    user_status = Status.get_user_status!(id)
    render(conn, "show.json", user_status: user_status)
  end

  def update(conn, %{"id" => id, "user_status" => user_status_params}) do
    user_status = Status.get_user_status!(id)

    with {:ok, %UserStatus{} = user_status} <- Status.update_user_status(user_status, user_status_params) do
      render(conn, "show.json", user_status: user_status)
    end
  end

  def delete(conn, %{"id" => id}) do
    user_status = Status.get_user_status!(id)

    with {:ok, %UserStatus{}} <- Status.delete_user_status(user_status) do
      send_resp(conn, :no_content, "")
    end
  end
end
