defmodule MockApp.Status do
  @moduledoc """
  The Status context.
  """

  import Ecto.Query, warn: false
  alias MockApp.Repo

  alias MockApp.Status.UserStatus

  @doc """
  Returns the list of user_status.

  ## Examples

      iex> list_user_status()
      [%UserStatus{}, ...]

  """
  def list_user_status do
    Repo.all(UserStatus)
  end

  @doc """
  Gets a single user_status.

  Raises `Ecto.NoResultsError` if the User status does not exist.

  ## Examples

      iex> get_user_status!(123)
      %UserStatus{}

      iex> get_user_status!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user_status!(id), do: Repo.get!(UserStatus, id)

  @doc """
  Creates a user_status.

  ## Examples

      iex> create_user_status(%{field: value})
      {:ok, %UserStatus{}}

      iex> create_user_status(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user_status(attrs \\ %{}) do
    %UserStatus{}
    |> UserStatus.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a user_status.

  ## Examples

      iex> update_user_status(user_status, %{field: new_value})
      {:ok, %UserStatus{}}

      iex> update_user_status(user_status, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user_status(%UserStatus{} = user_status, attrs) do
    user_status
    |> UserStatus.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a UserStatus.

  ## Examples

      iex> delete_user_status(user_status)
      {:ok, %UserStatus{}}

      iex> delete_user_status(user_status)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user_status(%UserStatus{} = user_status) do
    Repo.delete(user_status)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user_status changes.

  ## Examples

      iex> change_user_status(user_status)
      %Ecto.Changeset{source: %UserStatus{}}

  """
  def change_user_status(%UserStatus{} = user_status) do
    UserStatus.changeset(user_status, %{})
  end
end
