defmodule MockApp.Repo do
  use Ecto.Repo,
    otp_app: :mock_example,
    adapter: Ecto.Adapters.Postgres
end
