defmodule MockApp.Status.UserStatus do
  use Ecto.Schema
  import Ecto.Changeset

  schema "user_status" do
    field :description, :string
    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(user_status, attrs) do
    user_status
    |> cast(attrs, [:name, :description])
    |> validate_required([:name, :description])
  end
end
