defmodule MockServerClient.MockServer do
  use Plug.Router
  plug(Plug.Parsers, parsers: [:json], pass: ["text/*"], json_decoder: Jason)
  plug :match
  plug :dispatch


  post "/auth/login" do
    {status, body} = 
        case conn.params do
          %{"password" => "123456", "userName" => "jhon.doe@peiky.com"} ->
            { 200, %{"id" => 1234, "name" => "jhon.doe@peiky.com"} }
           %{"password" => "123", "userName" => "jhon.doe@peiky.com"} ->
            {404, %{"error" => "error"}}
        end
    send_resp(conn, status, Jason.encode!(body))
  end

end