defmodule MockServerClient.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children =[
      Plug.Cowboy.child_spec( scheme: :http, plug: MockServerClient.MockServer, options: [port: 8081]),
      MockApp.Repo,
      MockAppWeb.Endpoint
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: MockServerClient.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    MockAppWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
