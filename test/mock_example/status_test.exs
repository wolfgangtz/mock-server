defmodule MockApp.StatusTest do
  use MockApp.DataCase

  alias MockApp.Status

  describe "user_status" do
    alias MockApp.Status.UserStatus

    @valid_attrs %{description: "some description", name: "some name"}
    @update_attrs %{description: "some updated description", name: "some updated name"}
    @invalid_attrs %{description: nil, name: nil}

    def user_status_fixture(attrs \\ %{}) do
      {:ok, user_status} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Status.create_user_status()

      user_status
    end

    test "list_user_status/0 returns all user_status" do
      user_status = user_status_fixture()
      assert Status.list_user_status() == [user_status]
    end

    test "get_user_status!/1 returns the user_status with given id" do
      user_status = user_status_fixture()
      assert Status.get_user_status!(user_status.id) == user_status
    end

    test "create_user_status/1 with valid data creates a user_status" do
      assert {:ok, %UserStatus{} = user_status} = Status.create_user_status(@valid_attrs)
      assert user_status.description == "some description"
      assert user_status.name == "some name"
    end

    test "create_user_status/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Status.create_user_status(@invalid_attrs)
    end

    test "update_user_status/2 with valid data updates the user_status" do
      user_status = user_status_fixture()
      assert {:ok, %UserStatus{} = user_status} = Status.update_user_status(user_status, @update_attrs)
      assert user_status.description == "some updated description"
      assert user_status.name == "some updated name"
    end

    test "update_user_status/2 with invalid data returns error changeset" do
      user_status = user_status_fixture()
      assert {:error, %Ecto.Changeset{}} = Status.update_user_status(user_status, @invalid_attrs)
      assert user_status == Status.get_user_status!(user_status.id)
    end

    test "delete_user_status/1 deletes the user_status" do
      user_status = user_status_fixture()
      assert {:ok, %UserStatus{}} = Status.delete_user_status(user_status)
      assert_raise Ecto.NoResultsError, fn -> Status.get_user_status!(user_status.id) end
    end

    test "change_user_status/1 returns a user_status changeset" do
      user_status = user_status_fixture()
      assert %Ecto.Changeset{} = Status.change_user_status(user_status)
    end
  end
end
