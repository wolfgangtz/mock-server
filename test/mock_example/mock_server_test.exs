defmodule MockServerClient.Test do
  use ExUnit.Case, async: false
  use Plug.Test
  @opts MockServerClient.MockServer.init([])

  @success_repo_params %{password: "123456", userName: "jhon.doe@peiky.com"}
  @failure_repo_params %{password: "123", userName: "jhon.doe@peiky.com"}

  test "login_payment_endpoint when success" do
    conn = conn(:post, "/auth/login", @success_repo_params)
    conn = MockServerClient.MockServer.call(conn, @opts)
    assert conn.status == 200
    assert Jason.decode!(conn.resp_body) == %{"id" => 1234, "name" => "jhon.doe@peiky.com"}
  end


  test "login_payment_endpoint when failed" do
    conn = conn(:post, "/auth/login", @failure_repo_params)
    conn = MockServerClient.MockServer.call(conn, @opts)
    assert conn.status == 404
    assert Jason.decode!(conn.resp_body) == %{"error" => "error"}
  end


  test "login_payment_endpoint when success using HTTPoison" do
    {:ok, %HTTPoison.Response{body: body, status_code: status}} = HTTPoison.post("http://localhost:8081/auth/login", Jason.encode!(@success_repo_params), [{"Content-Type", "application/json"}])
    assert status == 200
    assert Jason.decode!(body) == %{"id" => 1234, "name" => "jhon.doe@peiky.com"}
  end


end