defmodule MockAppWeb.UserStatusControllerTest do
  use MockAppWeb.ConnCase

  alias MockApp.Status
  alias MockApp.Status.UserStatus

  @create_attrs %{
    description: "some description",
    name: "some name"
  }
  @update_attrs %{
    description: "some updated description",
    name: "some updated name"
  }
  @invalid_attrs %{description: nil, name: nil}

  def fixture(:user_status) do
    {:ok, user_status} = Status.create_user_status(@create_attrs)
    user_status
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all user_status", %{conn: conn} do
      conn = get(conn, Routes.user_status_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create user_status" do
    test "renders user_status when data is valid", %{conn: conn} do
      conn = post(conn, Routes.user_status_path(conn, :create), user_status: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.user_status_path(conn, :show, id))

      assert %{
               "id" => id,
               "description" => "some description",
               "name" => "some name"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.user_status_path(conn, :create), user_status: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update user_status" do
    setup [:create_user_status]

    test "renders user_status when data is valid", %{conn: conn, user_status: %UserStatus{id: id} = user_status} do
      conn = put(conn, Routes.user_status_path(conn, :update, user_status), user_status: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.user_status_path(conn, :show, id))

      assert %{
               "id" => id,
               "description" => "some updated description",
               "name" => "some updated name"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, user_status: user_status} do
      conn = put(conn, Routes.user_status_path(conn, :update, user_status), user_status: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete user_status" do
    setup [:create_user_status]

    test "deletes chosen user_status", %{conn: conn, user_status: user_status} do
      conn = delete(conn, Routes.user_status_path(conn, :delete, user_status))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.user_status_path(conn, :show, user_status))
      end
    end
  end

  defp create_user_status(_) do
    user_status = fixture(:user_status)
    {:ok, user_status: user_status}
  end
end
