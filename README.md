# MockApp

In this project the idea is create a mock server usign cowboy_plug to test our gateway endpoints.

# Create project
The first step is create a simple project, to do that run this in your command line:

```
mix phx.new mock_example --app mock_example --module MockApp --no-html --no-webpack
```
this example was made using:

```
Erlang/OTP 22 [erts-10.4.4] [source] [64-bit] [smp:8:8] [ds:8:8:10] [async-threads:1] [hipe] [dtrace]
Elixir 1.9.1 (compiled with Erlang/OTP 20)
```

and phoenix 1.4.9 version, if you need help to update phoenix version, this link will help a lot: https://phoenixframework.org/blog/phoenix-1-4-0-released

# Create a simple context

the next step is create a simple context, in order to auto-generate code:

```
cd mock_example
mix phx.gen.json Status UserStatus user_status name:string description:string
```
the previous command line will create a context "user status", if you need some help about context and how it work please check this link: https://hexdocs.pm/phoenix/contexts.html

# Once the context was created ..

In this step, you will have all the project structure created, basically is something like that:
  * in lib/mock_example we will have a folder for the context where will be a unique file with the schema.
  * in lib/mock_example we will have a file for the context with the basic operations over the databases (CRUD)
  * in lib/mock_example_web/controllers will be a controller for the context created, here, using the auto-generated files in lib/mock_example will have the CRUD of each schema, in the controllers will be a high level abstraction of the context.
  * in priv/repo/migrations we will have all the auto-generated migrations for each schema created (using context command).
  * in test we will have all the auto-generated test for the controllers created.

now, you will need to complement the router.ex with the endpoint definitions, to do that add this lines:

```
  scope "/api", MockAppWeb do
    pipe_through :api
    resources "/user_status", UserStatusController, except: [:new, :edit]
  end
```

# Create and configure development database 

To do that, please run `mix ecto.create`, install dependecies `mix deps.get` and regenerate migrations `mix ecto.migrate`. If you have ano trouble in this step please check that the configuration of the database in `config/dev.exs` and `config/test.exs` can be executed on your local environment.

# Run tests

In this step, we will verify that all the logic auto-generated work fine running the tests:

```
mix test
```

the output should be something like this:
```
................

Finished in 0.3 seconds
16 tests, 0 failures

Randomized with seed 120120
```

# INIT CONFIGURATION OF MOCK SERVER

in this step, we have a project than work fine and we will create a mock server in order to test the local endpoints simulating the data that will receive from external endpoints, ad verifiy if our code work as expected.

now, edit the config file in `config/config.exs` and and in the botton inf the file this line:

```
config :mock_example, env: Mix.env
```

with the line added above, we can access to the current environment, open the iex and try this:

```
$ iex -S mix                           
Erlang/OTP 22 [erts-10.4.4] [source] [64-bit] [smp:8:8] [ds:8:8:10] [async-threads:1] [hipe] [dtrace]

Compiling 18 files (.ex)
Test mode
Generated mock_example app
Interactive Elixir (1.9.1) - press Ctrl+C to exit (type h() ENTER for help)
iex(1)> Application.get_env(:mock_example, :env)
:dev
```

now, we will tell to our application to run Cowboy and Plug in the test environment only, to do that, open the file mix.exs and edit the application function:

```
  def application do
    #Get environment var:
    env = Application.get_env(:mock_example, :env)
    if env == :test do
      IO.puts "Test mode"
      [
        mod: {MockServerClient.Application, []},
        extra_applications: [:logger, :runtime_tools],
      ]
    else
      [
        mod: {MockApp.Application, []},
        extra_applications: [:logger, :runtime_tools],
      ]
    end

  end
```

then, We want our server to do a few things for us:

* Start up in a supervision tree when the application starts (in the test env only)
* Handle and respond to incoming web requests.

In order to make sure we can start up our test server as part of our application’s supervision tree, it will need to implement the GenServer API’s init and start_link functions. Luckily for us, we’ll get that behavior by using the Plug.Router module. create a `mock_server.ex` file in `lib/mock_example/mock_server` path, and write this code:

```
defmodule MockServerClient.MockServer do
  use Plug.Router
end
```

Now, we can tell our application to start up and supervise the Cowboy web server when the app starts up, We’ll do so with the `Plug.Cowboy.child_spec/1` function.
This function expects three options:
* :scheme - HTTP or HTTPS as an atom (:http, :https)
* :plug - The plug module to be used as the interface for the web server. You can specify a module name, like MyPlug, or a tuple of the module name and options {MyPlug, plug_opts}, where plug_opts gets passed to your plug modules init/1 function.
* :options - The server options. Should include the port number on which you want your server listening for requests.

now, createthe file `lib/mock_example/mock_server/application.ex`, the function start:

```
defmodule MockServerClient.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children =[
      Plug.Cowboy.child_spec( scheme: :http, plug: MockServerClient.MockServer, options: [port: 8081]),
      MockApp.Repo,
      MockAppWeb.Endpoint
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: MockServerClient.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    MockAppWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
```

Now that our server knows how to start up, let’s build out the ApiMockServer that is the interface for our web requests. We will give it some routes and teach it how to respond to certain web requests. Since our controller will be receiving JSON payloads, we will also tell it to run requests through the Plug.Parsers plug. This will parse the request body for us, to do that, edit the file `mock_server.ex`:

```
defmodule MockServerClient.MockServer do
  use Plug.Router
  plug Plug.Parsers, parsers: [:json], pass: ["text/*"], json_decoder: Jason
  plug :match
  plug :dispatch
end
```
Now we are ready to add our routes !!, Eventually, we will need to add routes that know how to handle the happy and sad paths for any web requests sent in the course of a test run. For now, we will revisit our earlier test example, which runs code that hits the `POST /auth/login` payment API endpoint, please, edit once more time the file `mock_server.ex`:

```
defmodule MockServerClient.MockServer do
  use Plug.Router
  plug(Plug.Parsers, parsers: [:json], pass: ["text/*"], json_decoder: Jason)
  plug :match
  plug :dispatch


  post "/auth/login" do
    {status, body} = 
        case conn.params do
          %{"password" => "123456", "userName" => "jhon.doe@peiky.com"} ->
            { 200, %{"id" => 1234, "name" => "jhon.doe@peiky.com"} }
           %{"password" => "123", "userName" => "jhon.doe@peiky.com"} ->
            {404, %{"error" => "error"}}
        end
    send_resp(conn, status, Jason.encode!(body))
  end

end
```
We want to test that, when we successfully do a login using payment API, the function returns a JSON struct with status 200. When we don’t successfully do a login via payment API, we return a JSON struct with status 400. Instead of defining complicated function mocks inside our tests, we will write our nice clean tests with no awareness of any mocks. Now we can write tests that are totally agnostic of any mocking, create the test file in `test/mock_example/mock_server_test.exs` and write the test:

```
defmodule MockServerClient.Test do
  use ExUnit.Case, async: false
  use Plug.Test
  @opts MockServerClient.MockServer.init([])

  @success_repo_params %{password: "123456", userName: "jhon.doe@peiky.com"}
  @failure_repo_params %{password: "123", userName: "jhon.doe@peiky.com"}

  test "login_payment_endpoint when success" do
    conn = conn(:post, "/auth/login", @success_repo_params)
    conn = MockServerClient.MockServer.call(conn, @opts)
    assert conn.status == 200
    assert Jason.decode!(conn.resp_body) == %{"id" => 1234, "name" => "jhon.doe@peiky.com"}
  end


  test "login_payment_endpoint when failed" do
    conn = conn(:post, "/auth/login", @failure_repo_params)
    conn = MockServerClient.MockServer.call(conn, @opts)
    assert conn.status == 404
    assert Jason.decode!(conn.resp_body) == %{"error" => "error"}
  end

end
```

Now, run the test `mix test`:

```
Compiling 21 files (.ex)
Test mode
Generated mock_example app
..................

Finished in 0.2 seconds
18 tests, 0 failures

Randomized with seed 859100
```

in the result of the test above, we can see that our code can run the auto-generated test and the new test, now, is tome to write the rest of test !!.

# Testing using HTTPoison
Now, we will add the HTTPoison library to do request using this library direclty to our mock server, to do that, add the dependency in the `mix.exs` file:

```
  defp deps do
    [
      {:phoenix, "~> 1.4.9"},
      {:phoenix_pubsub, "~> 1.1"},
      {:phoenix_ecto, "~> 4.0"},
      {:ecto_sql, "~> 3.1"},
      {:postgrex, ">= 0.0.0"},
      {:gettext, "~> 0.11"},
      {:jason, "~> 1.0"},
      {:plug_cowboy, "~> 2.0"},
      {:httpoison, "~> 1.5"}
    ]
  end
```

remember install the dependency running the command `mix deps.get`, after install our dependency, we will write the new test, please in the `test/mock_example/mock_server_test.exs` file add this test:

```
  test "login_payment_endpoint when success using HTTPoison" do
    {:ok, %HTTPoison.Response{body: body, status_code: status}} = HTTPoison.post("http://localhost:8081/auth/login", Jason.encode!(@success_repo_params), [{"Content-Type", "application/json"}])
    assert status == 200
    assert Jason.decode!(body) == %{"id" => 1234, "name" => "jhon.doe@peiky.com"}
  end
```

Now, run the test `mix test`:

```
..................

Finished in 0.2 seconds
19 tests, 0 failures

Randomized with seed 487401
```


# Resources
This app was created based on this tutorials:
* https://dev.to/jonlunsford/elixir-building-a-small-json-endpoint-with-plug-cowboy-and-poison-1826
* https://medium.com/flatiron-labs/rolling-your-own-mock-server-for-testing-in-elixir-2cdb5ccdd1a0

dependencies added:
* https://github.com/edgurgel/httpoison

Important links:
 * If you have a this error: ```** (Plug.Conn.AlreadySentError) the response was already sent ``` you can solve this using the ```halt(conn)``` function. https://github.com/dwyl/hits-elixir/issues/2
