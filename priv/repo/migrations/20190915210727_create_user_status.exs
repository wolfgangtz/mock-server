defmodule MockApp.Repo.Migrations.CreateUserStatus do
  use Ecto.Migration

  def change do
    create table(:user_status) do
      add :name, :string
      add :description, :string

      timestamps()
    end

  end
end
